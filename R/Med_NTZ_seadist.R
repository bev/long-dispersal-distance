# Calculate geographic isolation of Mediterranean MPA NTZs (No Take Zones = reserves)

## 1. get Mediterranean NTZ centroid coordinates. Polygons from Abecasis et al. 2023, centroids extracted and adjusted in QGIS 3.10.11
## 2. calculate sea distance as minimum distance between centroids with infinite resistance on land
## 3. extract nearest neighbour distnace for each NTZ


## load packages
library(dplyr)
library(raster)
library(sp)
library(gdistance)

## load data

# Mediterranean NTZ centroids (does not consider separate NTZ per MPA)
ntz_centroid <- read.csv("data/MPAFinalLIst_centroid_rasterAdj.csv") %>% 
  dplyr::select(NAME, Area, xcoord, ycoord)
head(ntz_centroid)

# raster of marine areas

load("data/SeaRaster.RData")

## calculate sea distance ----

# Create transition layer. 1/mean(x) generates a conductance layer. 
t <- transition(SeaRaster, function(x) 1/mean(x), directions=8) 

# Correct transition layer for geography. "c" indicates "least cost" method. 
tc <- geoCorrection(t, type="c") 

# attribute projection
crs.geo <- CRS("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0")  # geographical, datum WGS84
coord <- ntz_centroid
coordinates(coord) <- c("xcoord", "ycoord")
proj4string(coord) <- crs.geo  # define projection system of our data
summary(coord)

# Find shortest path and output SpatialLines object 
seadist <-  costDistance(tc, coord) #calculates least-cost distance between points
seadist <- as.matrix(seadist) # distance is in meters
# check in km
seadist[31,32]/1000 # cap couronne et carry : 10.6
seadist[30,31]/1000 # banyuls et cap couronne : 188.4
seadist[32,26]/1000 # carry et calanques : 26.9


  # # plot specific points to check if on land
  # ntz_centroid[c(32,26),]
  # my_window <- extent(13, 14, 45, 46)
  # plot(my_window, col=NA)
  # plot(SeaRaster, add=T)
  # plot(coord[41,],add=T)

# export matrix
write.csv(seadist, file = "output/MedNTZ_seadist.csv")


## extract nearest neigbour ----

dist_mat <- seadist
diag(dist_mat) <- NA
neighbour_dist <- apply(dist_mat,2,min,na.rm=T)
neighbour_dist <- neighbour_dist /1000

ntz_centroid$min_seadist <- neighbour_dist

write.csv(ntz_centroid, "output/MedNTZ_neighbourdist.csv")

## Statistics on nearest-neighbour distances ----

# Statistics on distances
summary(ntz_centroid$min_seadist)
mean(ntz_centroid$min_seadist)
hist(ntz_centroid$min_seadist, breaks = 10, main = "Mediterranean no-take MPA isolation",
     xlab = "Nearest neighbour marine distance (km)")
abline(v=42, col = "red", lty = 5, lwd = 2)
abline(v=59.7, lwd = 2)
abline(v=77.4, lwd = 2, col = "darkgrey")
text("42 km", x = 50.8, y = 14.38, col = "red")
text("median = 59.7 km", x = 84.5, y = 13, col = "black")
text("mean = 77.4 km", x = 102, y = 6, col = "darkgrey")

# Largest distances
head(ntz_centroid[order(ntz_centroid$min_seadist,decreasing=T),])

# Fraction of MRs with distances smaller than the one of the literature review
length(which(ntz_centroid$min_seadist<42)) / length(ntz_centroid$min_seadist) * 100
length(which(ntz_centroid$min_seadist>=42)) / length(ntz_centroid$min_seadist) * 100
summary(ntz_centroid$min_seadist[which(ntz_centroid$min_seadist>=42)])



