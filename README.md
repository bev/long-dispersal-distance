# Revisiting long-distance dispersal in a coastal marine fish (Ecography)
Authors: Stéphanie Manel, Emilie Boulanger, Laura Benestan, David Mouillot, Alicia Dalongeville 

# Date creation: 
31/05/2023


***

## Description
We identified five pairs of closely related individuals in an exploited Mediterranean species, the white seabream (Diplodus sargus) using  six relatedness estimates coupled to a parentage assignment program. Genetic data were SNPs.  Those five pairs were all distant from more than 1300 km 
We provide the codes  1) to estimate kinship distances ; 2) to  build of figure 1 in the paper and 3) to estimate the marine distances between Marine Protected Areas. 

All R scripts are in the directory R and the data used with the R script in the directory Data

## Data 
#SNP data
Script to prepare GBS data for analysis are at https://gitlab.mbb.univ-montp2.fr/seaconnect
Raw SNP data used in this paper are available at Dryad Digital Repository: https://doi.org/10.5061/dryad.ffbg79cvs
 

## 1) Relatedness estimation  
We used the R package "related" to estimate the 6 relatedness distances.
The "generic" R script is available at  https://github.com/laurabenestan/Finescale_ibd/blob/main/05-long_distance/script-coancestry.R

## 2) Figure 1
R script to do  figure 1 :  Figure1.R. 
The data files are in the directory DATA


## 3) Estimating  the  nearest neighbours distances between no-take Marine Protected Areas  in the Mediteraneean Sea 
R script to estimate the distance among MPA :  Med_NTZ_seadist.R
The data files are in the directory DATA

## License
Cite the paper if you use the code.
Revisiting long-distance dispersal in a coastal marine fishe (Ecography)
Manel S,  Boulanger E, Benestan L, Mouillot D, Dalongeville A (2023) Revisiting long-distance dispersal in a coastal marine fish. Ecography.




